
var empolyee = [];

empolyee[0] = {
    firstName: "Марсель",
    secondName: "Шакирзянов",
    salary: 15000,
    month: "Декабрь"
}
empolyee[1] = {
    firstName: "Марсель",
    secondName: "Шакирзянов",
    salary: 0,
    month: "Январь"
}
empolyee[2] = {
    firstName: "Марсель",
    secondName: "Шакирзянов",
    salary: 15000,
    month: "Февраль"
}

/// <summary> Метод для определения была ли в запрашиваемый месяц выплачена зарплата.</summary>
function onClick() {
    var input = prompt("Введите название месяца чтобы узнать выплаты:");
    var index = 0;
    for (i = 0; i < empolyee.length; i++) {
        if (input === empolyee[i].month) {
            index = i;
            continue;
        }
        else {
            index = -1;
        }
    }
    var result = index != -1 && empolyee[index].salary > 0 ? alert("Выплачено:" + String(empolyee[index].salary))
        : alert("Выплата не была произведена:")

    var input = prompt("Зарплата:", "");
    var result = empolyee[index].salary == 0 && Boolean(empolyee[index].salary) == true ? empolyee[index] = 1000 + input : empolyee[index];

}


/// <summary> Метод заполнения таблицы данными из объекта.</summary>
function onPushTable() {

    for (i = 0; i < empolyee.length; i++) {
        var tr = document.createElement("tr");

        table.appendChild(tr);

        var firstName = document.createElement("th");
        var lastName = document.createElement("th");
        var salary = document.createElement("th");
        var month = document.createElement("th");

        tr.appendChild(firstName);
        tr.appendChild(lastName);
        tr.appendChild(salary);
        tr.appendChild(month);

        firstName.appendChild(document.createTextNode(empolyee[i].firstName));
        lastName.appendChild(document.createTextNode(empolyee[i].secondName));
        salary.appendChild(document.createTextNode(empolyee[i].salary));
        month.appendChild(document.createTextNode(empolyee[i].month));
    }

}

/// <summary> Метод заполнения таблицы из формы.</summary>
function insertTable() {

    empolyee[empolyee.length] = {
        firstName: document.getElementById('name').value,
        secondName: document.getElementById('secondName').value,
        salary: Number(document.getElementById('salary').value),
        month: document.getElementById('month').value
    }

    var table = document.getElementById("table");
    var tr = document.createElement("tr");
    table.appendChild(tr);

    var firstName = document.createElement("th");
    var lastName = document.createElement("th");
    var salary = document.createElement("th");
    var month = document.createElement("th");

    tr.appendChild(firstName);
    tr.appendChild(lastName);
    tr.appendChild(salary);
    tr.appendChild(month);

    firstName.appendChild(document.createTextNode(empolyee[empolyee.length - 1].firstName));
    lastName.appendChild(document.createTextNode(empolyee[empolyee.length - 1].secondName));
    salary.appendChild(document.createTextNode(empolyee[empolyee.length - 1].salary));
    month.appendChild(document.createTextNode(empolyee[empolyee.length - 1].month));
}