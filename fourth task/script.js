

/// <summary> Замыкание. </summary>
function onClick(value) {
    var name = 5 + value;
    alert(name);

}

var operator = "-";
/// <summary> Модуль через замыкание. </summary>
function changeOperator() {
    alert(operator);
}
changeOperator();


var hello = new Function('a,b', ' return a+b; ');

function onClickHello() {
    var input =prompt("Введите имя и фамилию","")
    var result = hello(input.split(' ')[0], input.split(' ')[1]);
    alert(result);
}