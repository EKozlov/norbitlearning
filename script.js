
/// <summary> Выводит диалоговое окно при нажатии на кнопку добавления марки. </summary>
function onClick() {

    var input = prompt("Введите название марки и страну производителя через пробел:", "");

    if (!input) {
        return;
    }

    var tr = document.createElement("tr");
    var li = document.createElement("li");
    
    cars.appendChild(tr);
    
    var model = document.createElement("th");
    var country = document.createElement("th");

    tr.appendChild(model);
    tr.appendChild(country);
    
    
    model.appendChild(document.createTextNode(input.split(' ')[0]));
    country.appendChild(document.createTextNode(input.split(' ')[1]));
    
}
